# Prueba Backend

Proyecto de prueba de backend para la empresa Haulmer

## Empezando

El proyecto está realizado en laravel 5.8, utilizando JWT para control de acceso y mockAPI para la persistencia de datos.

### Prerequisitos

Se necesita instalar apache, php y composer. Así como también algún cliente para enviar peticiones HTTP como Postman


### Instalación

El código fuente se encuentra en bitbucket, se encuentra público por lo que no debería haber problemas con las ssh key

```
git clone git@bitbucket.org:fdo36/test-haulmer.git
```

De todas formas, por HTTPS sería de la siguiente manera:

```
git clone https://fdo36@bitbucket.org/fdo36/test-haulmer.git
```

Luego de tener el proyecto clonado, instalar dependencias

```
composer install
```

En algunos casos, también sería necesario hacer una actualización de las dependencias

```
composer update
```

Y listo, el proyecto está instalado.

## Ejecutando el servidor

Para ejecutar el servidor, se utiliza "artisan", provisto por laravel

```
php artisan serve
```

y quedará ejecutándose en la siguiente dirección

```
http://127.0.0.1:8000
```

el API estará disponible, entonces, en la siguiente url:

```
http://127.0.0.1:8000/api/
```

### Endpoints

Los endpoints disponibles son:

* /new: crea un usuario nuevo
* /login: inicia sesión con las credenciales del usuario. Retorna el JWT para utilizar los demás endpoints (este jwt solo servirá por 60 segundos, después habrá que llamar a /login para obtener uno nuevo).
* /me: muestra información del usuario actual (necesita el envío del JWT obtenido en el login).
* /update: actualiza la información del usuario actual (necesita el envío del JWT obtenido en el login).

Para más detalles, consultar el archivo test-api.raml

Existen 2 usuarios de prueba en mockapi.io:

```
[
  {
    "id": "1",
    "name": "Bruno",
    "email": "bruno@email.com",
    "password": "1234"
  },
  {
    "id": "2",
    "name": "Fernando",
    "email": "fernando@email.com",
    "password": "1234"
  }
]
```


### Clases utilizadas

Para el modelo:

* MyJWT: clase para manejar instancias del JWT y atributos necesarios.
* MyUser: clase para manejar instancias de los usuarios. Es una clase customisada por lo que se prefirió crear una de cero.
* MyModel: clase padre para los modelos anteriores. Maneja funciones para guardar y actualizar información.
* RequestUtils: clase de utilidades para las requests. Usa curl y se comunica directamente con el API de mockapi.io para la persistencia de datos

Para el controlador:

* AuthController: clase controlador que posee las funciones literales de los endpoints a utilizar.
* MyUserController: clase para manejar las búsquedas de los usuarios dentro de la API de mockapi.io.
* MyJWTController: clase para manejar las búsquedas de los JWT dentro de la API de mockapi.io.


## Construido con

* [Laravel](https://laravel.com/) - El framework usado
* [Composer](https://getcomposer.org/) - Gestor de dependencias para PHP
* [Mockapi](https://www.mockapi.io) - Utilizado para persistencia de datos



## Authors

* **Fernando Torres** - *Ingeniero Civil en Computación*