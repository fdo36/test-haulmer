<?php

namespace App\Http\Controllers;

use App\MyJWT;
use App\RequestUtils;

class MyJWTController extends Controller
{
	/**
	 * Retorna un nuevo JWT segun el id en cuestión.
	 * implementación obtenida de: https://dev.to/robdwaller/how-to-create-a-json-web-token-using-php-3gml
	 *
	 * @param $id
	 * @return string
	 */
	public static function createJWT( $id )
	{

		// Create token header as a JSON string
		$header = json_encode( [ 'typ' => 'JWT', 'alg' => 'HS256' ] );

		// Create token payload as a JSON string
		$payload = json_encode( [ 'user_id' => $id ] );

		// Encode Header to Base64Url String
		$base64UrlHeader = str_replace( [ '+', '/', '=' ], [ '-', '_', '' ], base64_encode( $header ) );

		// Encode Payload to Base64Url String
		$base64UrlPayload = str_replace( [ '+', '/', '=' ], [ '-', '_', '' ], base64_encode( $payload ) );

		// Create Signature Hash
		$signature = hash_hmac( 'sha256', $base64UrlHeader . "." . $base64UrlPayload, 'abC123!', true );

		// Encode Signature to Base64Url String
		$base64UrlSignature = str_replace( [ '+', '/', '=' ], [ '-', '_', '' ], base64_encode( $signature ) );

		// Create JWT
		$jwt = $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;

		return $jwt;
	}

	/**
	 * retorna un JWT activo para el usuario
	 *
	 * @param $userId
	 * @return mixed
	 */
	public static function handleJWT( $userId )
	{

		$response = MyJWT::find( RequestUtils::$API_TOKEN, $userId );

		$response = json_decode( $response, true );

		if ( $response ){
			foreach ( $response as $r ){
				if ( $r[ 'active' ] && $r[ 'userId' ] == $userId ){
					return $r[ 'jwt' ];
				}
			}
		}

		$myJWT = new MyJWT();
		$myJWT->jwt =  self::createJWT( $userId );
		$myJWT->createdAt = time();
		$myJWT->active = true;
		$myJWT->userId = $userId;

		return $myJWT->save();

	}

	/**
	 * Busca el jwt en cuestión.
	 *
	 * @param $jwt
	 * @return MyJWT|null
	 */
	public static function findByJwt( $jwt )
	{
		$response = MyJWT::find( RequestUtils::$API_TOKEN, $jwt );

		$response = json_decode( $response, true );

		if ( $response ){
			foreach ( $response as $r ){
				if ( $r[ 'active' ] && $r[ 'jwt' ] == $jwt ){
					$myJWT = new MyJWT();
					$myJWT->jwt =  $jwt;
					$myJWT->createdAt = $r[ 'createdAt' ];
					$myJWT->active = $r[ 'active' ];
					$myJWT->userId = $r[ 'userId' ];
					$myJWT->id = $r[ 'id' ];

					return $myJWT;
				}
			}
		}

		return null;
	}
}
