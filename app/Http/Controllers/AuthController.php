<?php

namespace App\Http\Controllers;


use App\MyJWT;
use App\MyUser;
use Illuminate\Http\Request;

class AuthController extends Controller
{

	/**
	 * inicia sesión y retorna el token
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function login( Request $request )
	{
		$email = $request->post( 'email', null );
		$password = $request->post( 'password', null );

		if ( !$email || !$password ) {
			return response()->json( [ 'message' => 'Debe ingresar el email password' ], 400 );
		}

		$user = MyUserController::findByEmail( $email );

		if ( $user === -1 ) {
			return response()->json( [ 'message' => 'Debe ingresar un email válido' ], 400 );
		}

		if ( !$user ) {
			return response()->json( [ 'message' => 'No existe usuario con ese email' ], 404 );
		}
		if ( $password === $user->password ) {
			$jwt = MyJWTController::handleJWT( $user->id );
			return response()->json( [ 'message' => 'Logeado satisfactoriamente', 'token' => isset( $jwt[ 'jwt' ] ) ? $jwt[ 'jwt' ] : $jwt ], 200 );
		}

		return response()->json( [ 'message' => 'Password incorrecta' ], 400 );
	}

	/**
	 * Obtiene el usuario logeado segun su jwt
	 *
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function me( Request $request )
	{
		$jwt = $request->post( 'jwt', null );

		if ( !$jwt ) {
			return response()->json( [ 'message' => 'Debe ingresar el jwt' ], 401 );
		}

		$myJwt = MyJWTController::findByJwt( $jwt );
		if ( !$myJwt ) {
			return response()->json( [ 'message' => 'Debe iniciar sesion' ], 401 );
		}

		if ( self::checkToken( $myJwt ) ){
			return response()->json( [ 'message' => 'Sesión expirada' ], 401 );
		}

		$user = MyUserController::findById( $myJwt->userId );
		return response()->json( $user, 200 );

	}


	/**
	 * crea un nuevo usuario.
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function new( Request $request )
	{
		$email = $request->post( 'email', null );
		$password = $request->post( 'password', null );
		$name = $request->post( 'name', null );

		if ( !$email || !$password || !$name ) {
			return response()->json( [ 'message' => 'Debe ingresar email, name y password' ], 400 );
		}

		if ( MyUserController::findByEmail( $email ) ) {
			return response()->json( [ 'message' => 'El email ya está registrado' ], 400 );
		}

		$user = new MyUser();
		$user->name = $name;
		$user->password = $password;
		$user->email = $email;

		return response()->json( $user->save(), 201 );
	}

	/**
	 * Actualiza la información de un usuario
	 *
	 * @param Request $request
	 * @param bool $id
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function update( Request $request, $id = false )
	{
		if ( !$id ) {
			return response()->json( [ 'message' => 'Debe ingresar el id del usuario a actualizar' ], 400 );
		}

		$jwt = $request->post( 'jwt', null );

		if ( !$jwt ) {
			return response()->json( [ 'message' => 'Debe ingresar el jwt' ], 401 );
		}

		$myJwt = MyJWTController::findByJwt( $jwt );
		if ( !$myJwt ) {
			return response()->json( [ 'message' => 'Debe iniciar sesión' ], 401 );
		}

		if ( self::checkToken( $myJwt ) ){
			return response()->json( [ 'message' => 'Sesión expirada' ], 401 );
		}

		$myUser = MyUserController::findById( $id );
		if ( !$myUser ) {
			return response()->json( [ 'message' => 'No existe el usuario' ], 404 );
		}

		if ( $id != $myJwt->userId ){
			return response()->json( [ 'message' => 'No tiene permitido continuar' ], 403 );
		}

		$newEmail = $request->post( 'email', null );

		$oldUser = MyUserController::findByEmail( $newEmail );

		if ( $oldUser && $oldUser->id != $id ){
			return response()->json( [ 'message' => 'Email ya utilizado' ], 400 );
		}

		$newName = $request->post( 'name', null );
		$newPassword = $request->post( 'password', null );


		$user = new MyUser();
		$user->name = !is_null( $newName ) && !empty( $newName ) ? $newName : $myUser->name;
		$user->password = !is_null( $newPassword ) && !empty( $newPassword ) ? $newPassword : $myUser->password;
		$user->email = !is_null( $newEmail ) && !empty( $newEmail ) ? $newEmail : $myUser->email;
		$user->id = $id;

		return response()->json( $user->update(), 200 );
	}

	/**
	 * deja inactivo el token
	 * @param MyJWT $myJwt
	 */
	private function revokeToken( $myJwt )
	{
		$myJwt->active = false;
		$myJwt->update();
	}

	/**
	 * verifica si han pasado los segundos indicados (hardcoded) para revocar el token
	 * @param MyJWT $myJwt
	 * @return bool
	 */
	private function checkToken( $myJwt )
	{
		if ( time() - $myJwt->createdAt > 60 ){ //60 segundos para revokar el token
			self::revokeToken( $myJwt );
			return true;
		}
		return false;
	}
}
