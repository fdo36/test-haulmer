<?php

namespace App\Http\Controllers;

use App\MyUser;
use App\RequestUtils;

class MyUserController extends Controller
{
	/**
	 * Retorna el usuario encontrado por email.
	 * Retorna null en caso de no encontrarlo.
	 *
	 * @param $email
	 * @return MyUser|int|null
	 */
	public static function findByEmail( $email )
	{
		$result = MyUser::find( RequestUtils::$API_USER, $email );
		if ( $result ) {

			$array = json_decode( $result, true );
			foreach ( $array as $r ) {
				if ( $r[ 'email' ] === $email ) {
					$user = new MyUser();
					$user->id = $r[ 'id' ];
					$user->name = $r[ 'name' ];
					$user->email = $r[ 'email' ];
					$user->password = $r[ 'password' ];
					return $user;
				}
			}
		}
		return null;
	}

	/**
	 * Retorna el usuario encontrado por id.
	 * Retorna null en caso de no encontrarlo.
	 *
	 * @param $id
	 * @return MyUser|int|null
	 */
	public static function findById( $id )
	{
		$result = MyUser::find( RequestUtils::$API_USER, $id );
		if ( $result ) {
			$array = json_decode( $result, true );
			foreach ( $array as $r ) {
				if ( $r[ 'id' ] == $id ){
					$user = new MyUser();
					$user->id = $r[ 'id' ];
					$user->name = $r[ 'name' ];
					$user->email = $r[ 'email' ];
					$user->password = $r[ 'password' ];
					return $user;
				}
			}
		}
		return null;
	}
}
