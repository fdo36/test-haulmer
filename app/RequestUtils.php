<?php

namespace App;


class RequestUtils
{

	public static $API_URL   = 'http://5d51db933432e70014e6b4ab.mockapi.io/api/v1/';
	public static $API_TOKEN = 'token';
	public static $API_USER  = 'user';
	public static $POST      = 'POST';
	public static $GET       = 'GET';
	public static $PUT       = 'PUT';

	/**
	 * realiza una petición http según el metodo indicado a la url dada.
	 * si es post, debería indicar los parámetros.
	 *
	 * modificación de lo obtenido de: https://stackoverflow.com/a/9802854/2574343
	 *
	 * @param string $method . POST, PUT, GET etc
	 * @param string $endpoint
	 * @param bool|array $data . array("param" => "value")
	 *
	 * @return mixed
	 */
	public static function callAPI( $method, $endpoint, $data = false )
	{
		$url = self::$API_URL . $endpoint;
		$curl = curl_init( $url );

		curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $curl, CURLINFO_HEADER_OUT, true );

		//si es post, si o si viene data
		if ( $method == "POST" ) {
			curl_setopt( $curl, CURLOPT_POST, true );

			$payload = json_encode( $data );
			curl_setopt( $curl, CURLOPT_POSTFIELDS, $payload );

			// Set HTTP Header for POST request
			//from: https://tecadmin.net/post-json-data-php-curl/
			curl_setopt( $curl, CURLOPT_HTTPHEADER,
				[
					'Content-Type: application/json',
					'Content-Length: ' . strlen( $payload )
				]
			);
		} else if ( $method == "PUT" ) {
			//from: https://stackoverflow.com/questions/21271140/curl-and-php-how-can-i-pass-a-json-through-curl-by-put-post-get
			curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, 'PUT' );

			$payload = json_encode( $data );
			curl_setopt( $curl, CURLOPT_POSTFIELDS, $payload );

			curl_setopt( $curl, CURLOPT_HTTPHEADER,
				[
					'Content-Type: application/json',
					'Content-Length: ' . strlen( $payload )
				]
			);
		}

		$result = curl_exec( $curl );

		curl_close( $curl );

		return $result;
	}
}
