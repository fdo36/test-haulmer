<?php

namespace App;


class MyModel
{
	public $id;

	/**
	 * Guarda el modelo en el mocakpi.io
	 * @return mixed
	 */
	public function save()
	{
		$endpoint = '';

		switch ( get_called_class() ) {
			case "App\MyJWT":
				$endpoint = RequestUtils::$API_TOKEN;
				break;
			case "App\MyUser":
				$endpoint = RequestUtils::$API_USER;
				break;
		}
		$data = [];
		foreach ( get_object_vars( $this ) as $key => $name ) {

			$data[$key] = $name;
		}


		return RequestUtils::callAPI( RequestUtils::$POST, $endpoint, $data );

	}

	/**
	 * Actualiza el modelo en el mocakpi.io
	 * @return mixed
	 */
	public function update()
	{
		$endpoint = '';

		switch ( get_called_class() ) {
			case "App\MyJWT":
				$endpoint = RequestUtils::$API_TOKEN;
				break;
			case "App\MyUser":
				$endpoint = RequestUtils::$API_USER;
				break;
		}
		$data = [];
		foreach ( get_object_vars( $this ) as $key => $name ) {

			$data[$key] = $name;
		}


		return RequestUtils::callAPI( RequestUtils::$PUT, $endpoint . '/' . $this->id, $data );

	}


	/**
	 * Encuentra el modelo segun un filtro utilizado por mocakpi.io
	 * @param $endpoint
	 * @param $filter
	 * @return mixed
	 */
	public static function find( $endpoint, $filter )
	{
		return RequestUtils::callAPI( RequestUtils::$GET, $endpoint . '?filter=' . $filter );
	}

	/**
	 * obtiene el modelo segun el id
	 *
	 * @param $endpoint
	 * @param $id
	 * @return mixed
	 */
	public static function findById( $endpoint, $id )
	{
		return RequestUtils::callAPI( RequestUtils::$GET, $endpoint . '/' . $id );
	}
}
